FROM ubuntu:trusty

# xterm is expected
ENV TERM=xterm

RUN apt-get update -y && apt-get install build-essential -y

ADD packages.txt /tmp/packages.txt
RUN xargs -a /tmp/packages.txt apt-get install -y

RUN pip3 install virtualenv
RUN /usr/local/bin/virtualenv /opt/docnow --distribute --python=/usr/bin/python3

ADD requirements.txt /tmp/requirements.txt

RUN /opt/docnow/bin/pip3 install -r /tmp/requirements.txt


RUN useradd --create-home --home-dir /home/docnow --shell /bin/bash docnow
RUN chown -R docnow /opt/docnow
RUN adduser docnow sudo

ADD run_luigi.sh /home/docnow
RUN chmod +x /home/docnow/run_luigi.sh
RUN chown docnow /home/docnow/run_luigi.sh

ADD activate_env /home/docnow/.bashrc

EXPOSE 8082
RUN usermod -a -G sudo docnow
RUN echo "docnow ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

USER docnow
RUN mkdir -p /home/docnow/dnflow
ENV SHELL=/bin/bash
ENV USER=docnow
VOLUME /home/docnow/dnflow
WORKDIR /home/docnow/dnflow
